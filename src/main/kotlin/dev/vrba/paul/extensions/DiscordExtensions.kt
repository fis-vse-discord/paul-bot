package dev.vrba.paul.extensions

import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Role

val Guild.everyone: Role
get() = this.getRoleById(this.id) ?: throw IllegalStateException("Cannot find the @everyone role")