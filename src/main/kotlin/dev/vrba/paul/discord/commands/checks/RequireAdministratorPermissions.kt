package dev.vrba.paul.discord.commands.checks

@Target(AnnotationTarget.CLASS)
annotation class RequireAdministratorPermissions
