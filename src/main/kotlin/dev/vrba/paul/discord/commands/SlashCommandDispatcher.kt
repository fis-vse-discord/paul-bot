package dev.vrba.paul.discord.commands

import dev.vrba.paul.discord.configuration.DiscordConfiguration
import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.DiscordLogger
import dev.vrba.paul.discord.commands.checks.RequireAdministratorPermissions
import dev.vrba.paul.discord.commands.checks.RequireVerification
import dev.vrba.paul.discord.modules.status.gitlab
import dev.vrba.paul.discord.modules.verification.configuration.VerificationConfiguration
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.MessageBuilder
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import net.dv8tion.jda.api.interactions.components.ActionRow
import net.dv8tion.jda.api.interactions.components.Button
import org.springframework.stereotype.Component
import java.time.Instant
import kotlin.reflect.full.hasAnnotation

@Component
class SlashCommandDispatcher(
    commands: List<SlashCommand>,
    private val logger: DiscordLogger,
    private val configuration: VerificationConfiguration,
    private val discordConfiguration: DiscordConfiguration
) : ListenerAdapter() {

    private val commands = commands.associateBy { it.definition.name }

    fun registerSlashCommands(guild: Guild) {
        guild.updateCommands()
            .addCommands(commands.values.map { it.definition })
            .queue()
    }

    override fun onSlashCommand(event: SlashCommandEvent) {
        val member = event.member ?: return logger.warning("Missing member event property")
        val handler = commands[event.name] ?: return logger.warning("Missing handler for command /${event.name}")

        try {
            if (!hasExecutionPermission(handler, member)) {
                return event.reply(missingPermissions()).queue()
            }

            handler.execute(event)
        }
        catch (exception: Throwable) {
            logger.exception(exception)

            val embed = commandExecutionFailed()
            val button = Button.link("${gitlab}/-/issues/new", "\uD83D\uDCE2 Nahlásit chybu")

            // The event was already replied to / deferred
            if (event.isAcknowledged) {
                event.hook.editOriginalEmbeds(embed).setActionRow(button).queue()
                return
            }

            // Otherwise, it is possible to reply directly
            event.replyEmbeds(embed).addActionRow(button).queue()
        }
    }

    private fun hasExecutionPermission(handler: SlashCommand, member: Member): Boolean {
        // Allow execution of all commands to users with MANAGE_SERVER permissions
        if (member.isOwner || member.hasPermission(Permission.MANAGE_SERVER)) {
            return true
        }

        // If the command is annotated as administrators-only, check the permissions
        if (handler::class.hasAnnotation<RequireAdministratorPermissions>() && !member.hasPermission(Permission.MANAGE_SERVER)) {
            return false
        }

        // If the command is annotated as verified users-only, check the verified role presence
        if (handler::class.hasAnnotation<RequireVerification>() && member.roles.none { it.idLong == configuration.verifiedRole }) {
            return false
        }

        return true
    }

    private fun missingPermissions(): Message {
        val embed = EmbedBuilder()
            .setColor(DiscordColors.red)
            .setTitle("Přístup zamítnut!")
            .setDescription(
                """
                |Pro spuštění toho příkazu nemáš dostatečná oprávnění.
                |Pokud si myslíš, že jde o chybu, napiš někomu z moderátorů nebo otevři nové issue na Gitlabu
                """.trimMargin()
            )
            .setTimestamp(Instant.now())
            .build()

        val row = ActionRow.of(
            Button.link("${gitlab}/-/issues/new", "\uD83D\uDCE2 Nahlásit chybu")
        )

        return MessageBuilder()
            .setEmbeds(embed)
            .setActionRows(row)
            .build()
    }

    private fun commandExecutionFailed(): MessageEmbed = EmbedBuilder()
        .setColor(DiscordColors.red)
        .setTitle("Command execution failed")
        .setDescription("Additional info with stack trace was sent to the <#${discordConfiguration.logChannel}> channel")
        .setTimestamp(Instant.now())
        .build()
}