package dev.vrba.paul.discord

import dev.vrba.paul.discord.configuration.DiscordConfiguration
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.entities.Guild
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

@Component
final class DiscordBeans(configuration: DiscordConfiguration) {
    @get:Bean
    val jda: JDA = JDABuilder.createDefault(configuration.token).build().awaitReady()

    @get:Bean
    val guild: Guild = jda.getGuildById(configuration.guild)
        ?: throw IllegalStateException("Cannot find the configured guild (${configuration.guild})")

    @get:Bean
    val logger: DiscordLogger = DiscordLogger(configuration, guild)
}