package dev.vrba.paul.discord

import dev.vrba.paul.discord.commands.SlashCommandDispatcher
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.Activity
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.hooks.ListenerAdapter
import org.springframework.stereotype.Service

@Service
class DiscordService(
    private val jda: JDA,
    private val guild: Guild,
    private val logger: DiscordLogger,
    private val dispatcher: SlashCommandDispatcher,
    private val listeners: List<ListenerAdapter>
) {

    fun start() {
        registerEventListeners()
        registerSlashCommands()
        updatePresence()
        logStartup()
    }

    private fun registerEventListeners() {
        listeners.forEach { jda.addEventListener(it) }
    }

    private fun registerSlashCommands() {
        dispatcher.registerSlashCommands(guild)
    }

    private fun updatePresence() {
        jda.presence.activity = Activity.watching(guild.name)
    }

    private fun logStartup() {
        logger.info("Bot instance started", "Managing the **${guild.name}** guild") {
            val user = jda.selfUser
            val runtime = Runtime.getRuntime()

            // TODO: Add git info
            it.setAuthor(user.name, null, user.avatarUrl)
                .addField("Logged in as", user.asMention, false)
                .addField("Heap memory", "`${runtime.totalMemory()}/${runtime.maxMemory()}`", false)
        }
    }
}