package dev.vrba.paul.discord.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "discord")
data class DiscordConfiguration(
    val token: String,
    val guild: Long,
    val logChannel: Long
)
