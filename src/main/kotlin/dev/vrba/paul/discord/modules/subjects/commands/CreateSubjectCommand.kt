package dev.vrba.paul.discord.modules.subjects.commands

import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.commands.SlashCommand
import dev.vrba.paul.discord.commands.checks.RequireAdministratorPermissions
import dev.vrba.paul.discord.modules.subjects.entities.Subject
import dev.vrba.paul.discord.modules.subjects.services.SubjectsService
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.CommandData
import org.springframework.stereotype.Component
import java.time.Instant

@Component
@RequireAdministratorPermissions
class CreateSubjectCommand(private val service: SubjectsService) : SlashCommand {

    override val definition: CommandData = CommandData("create-subject", "Create a new subject with associated role and channels")
        .addOption(OptionType.STRING, "code", "Code of the subject, needs to be unique", true)
        .addOption(OptionType.STRING, "name", "Name of the subject", true)

    override fun execute(event: SlashCommandEvent) {
        val code = event.getOption("code")?.asString ?: throw IllegalArgumentException("Missing the code argument")
        val name = event.getOption("name")?.asString ?: throw IllegalArgumentException("Missing the name argument")

        val interaction = event.deferReply().complete()
        val subject = service.createSubject(code, name)

        interaction.editOriginalEmbeds(subjectCreated(subject)).queue()
    }

    private fun subjectCreated(subject: Subject): MessageEmbed = EmbedBuilder()
        .setTitle("Subject `${subject.code}` created")
        .setColor(DiscordColors.green)
        .setTimestamp(Instant.now())
        .build()
}