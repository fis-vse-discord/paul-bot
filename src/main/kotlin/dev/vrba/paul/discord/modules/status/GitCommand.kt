package dev.vrba.paul.discord.modules.status

import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.commands.SlashCommand
import dev.vrba.paul.discord.commands.checks.RequireVerification
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.build.CommandData
import net.dv8tion.jda.api.interactions.components.Button
import org.springframework.stereotype.Component


const val gitlab = "https://gitlab.com/fis-vse-discord/paul-bot"

@Component
@RequireVerification
class GitCommand : SlashCommand {

    override val definition: CommandData = CommandData("git", "Provides git information of the deployed instance")

    override fun execute(event: SlashCommandEvent) {
        event.replyEmbeds(gitEmbed())
            .addActionRow(Button.link(gitlab, "Repository"))
            .complete()
    }

    private fun gitEmbed(): MessageEmbed = EmbedBuilder()
        .setTitle("Git info")
        .setColor(DiscordColors.git)
        .addField("Gitlab repository", gitlab, false)
        // TODO: Add information about current branch and commit...
        .build()
}