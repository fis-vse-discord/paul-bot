package dev.vrba.paul.discord.modules.socialcredit.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "social-credit")
data class SocialCreditConfiguration(
    val defaultCredit: Long,
    val plusEmojiId: Long,
    val minusEmojiId: Long
)