package dev.vrba.paul.discord.modules.verification.entities

import org.hibernate.Hibernate
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "verifications")
data class Verification(
    @Id
    @Column(name = "id")
    val id: UUID = UUID.randomUUID(),

    @Column(name = "member_id", nullable = false, unique = true)
    val memberId: Long,

    @Column(name = "azure_id", nullable = true, unique = true)
    var azureId: String? = null,

    @Column(name = "revoked")
    var revoked: Boolean = false
) {
    // Methods optimized for Hibernate performance
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        if (other !is Verification) return false

        return id == other.id
    }

    override fun hashCode(): Int = 0

    override fun toString(): String = this::class.simpleName + "(id = $id)"
}
