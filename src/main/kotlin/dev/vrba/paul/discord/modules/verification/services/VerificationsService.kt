package dev.vrba.paul.discord.modules.verification.services

import dev.vrba.paul.discord.DiscordLogger
import dev.vrba.paul.discord.modules.verification.configuration.VerificationConfiguration
import dev.vrba.paul.discord.modules.verification.entities.Verification
import dev.vrba.paul.discord.modules.verification.exceptions.AzureIdMismatch
import dev.vrba.paul.discord.modules.verification.exceptions.BlockedGroupMembership
import dev.vrba.paul.discord.modules.verification.exceptions.RevokedVerification
import dev.vrba.paul.discord.modules.verification.exceptions.VerificationNotFound
import dev.vrba.paul.discord.modules.verification.repositories.VerificationsRepository
import net.dv8tion.jda.api.entities.Guild
import org.springframework.data.repository.findByIdOrNull
import org.springframework.security.oauth2.core.user.OAuth2User
import org.springframework.stereotype.Service
import java.util.*

@Service
class VerificationsService(
    private val guild: Guild,
    private val logger: DiscordLogger,
    private val configuration: VerificationConfiguration,
    private val repository: VerificationsRepository
) {
    fun getMemberVerification(member: Long): Verification {
        return repository.findByMemberId(member) ?: createVerification(member)
    }

    fun processMemberVerification(id: UUID, user: OAuth2User): Boolean {
        try {
            val azureId = user.getAttribute<String>("oid")
            val azureGroups = user.getAttribute<List<String>>("groups")

            if (azureId == null || azureGroups == null) {
                throw IllegalStateException("Missing either `oid` or `groups` Azure AD parameter. WTF?")
            }

            val verification = repository.findByIdOrNull(id) ?: throw VerificationNotFound()

            // The user is member of one or more of the blocked Azure AD roles
            if (azureGroups.any { configuration.blockedRoles.contains(it) }) {
                throw BlockedGroupMembership()
            }

            // The verification was used by another user already
            if (verification.azureId != null && verification.azureId != azureId) {
                throw AzureIdMismatch()
            }

            if (verification.revoked) {
                throw RevokedVerification()
            }

            val role = guild.getRoleById(configuration.verifiedRole) ?: throw IllegalStateException("Cannot find the verified role.")

            repository.save(verification.apply { this.azureId = azureId })
            guild.addRoleToMember(verification.memberId, role).complete()

            logger.success("Verification succeeded", null) {
                it.addField("Verification Id", "`${id}`", false)
                    .addField("User", "<@${verification.memberId}>", false)
            }
        }
        catch (exception: Exception) {
            logger.error("Verification failed", null) { it.addField("Verification ID", "`${id}`", false) }
            logger.exception(exception)
            return false
        }

        return true
    }

    private fun createVerification(member: Long): Verification {
        val verification = Verification(memberId = member)

        logger.info("Created a new verification", null) {
            it.addField("Verification ID", "`${verification.id}`", false)
                .addField("User", "<@${member}>", false)
        }

        return repository.save(verification)
    }
}