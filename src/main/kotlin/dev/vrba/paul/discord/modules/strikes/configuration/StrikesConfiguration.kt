package dev.vrba.paul.discord.modules.strikes.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "strikes")
data class StrikesConfiguration(
    val channel: Long
)