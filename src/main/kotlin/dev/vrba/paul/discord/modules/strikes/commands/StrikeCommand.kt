package dev.vrba.paul.discord.modules.strikes.commands

import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.commands.SlashCommand
import dev.vrba.paul.discord.commands.checks.RequireAdministratorPermissions
import dev.vrba.paul.discord.modules.strikes.entities.Strike
import dev.vrba.paul.discord.modules.strikes.services.StrikesService
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.CommandData
import org.springframework.stereotype.Component
import java.time.Instant

@Component
@RequireAdministratorPermissions
class StrikeCommand(private val service: StrikesService) : SlashCommand {
    override val definition: CommandData = CommandData("strike", "Gives a strike to the specified member. If the member has 3 strikes, he's banned automatically")
        .addOption(OptionType.USER, "member", "Member, that should be given the strike", true)
        .addOption(OptionType.STRING, "reason", "Reason for the strike, optimally a message link", false)

    override fun execute(event: SlashCommandEvent) {
        val admin = event.member ?: throw IllegalStateException("Missing the member attribute on SlashCommandEvent")
        val member = event.getOption("member")?.asMember ?: throw IllegalArgumentException("Missing the member parameter")
        val reason = event.getOption("reason")?.asString ?: "No reason provided"

        val interaction = event.deferReply().complete()

        val strike = service.createStrike(member, admin, reason)

        interaction.editOriginalEmbeds(strikeEmbed(strike)).queue()
    }

    private fun strikeEmbed(strike: Strike): MessageEmbed = EmbedBuilder()
        .setTitle("Strike ${strike.index} / 3 given")
        .setColor(DiscordColors.green)
        .addField("Member", "<@${strike.member}>", true)
        .addField("Admin", "<@${strike.admin}>", true)
        .addField("Reason", strike.reason, false)
        .setTimestamp(Instant.now())
        .build()
}