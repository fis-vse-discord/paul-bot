package dev.vrba.paul.discord.modules.status

import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.commands.SlashCommand
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.build.CommandData
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class PingCommand : SlashCommand {

    override val definition: CommandData = CommandData("ping", "Simple check that the bot is up and running")

    override fun execute(event: SlashCommandEvent) {
        event.replyEmbeds(
            EmbedBuilder()
                .setTitle("\uD83C\uDFD3 Pong!")
                .setColor(DiscordColors.primary)
                .setTimestamp(Instant.now())
                .build()
        ).queue()
    }
}