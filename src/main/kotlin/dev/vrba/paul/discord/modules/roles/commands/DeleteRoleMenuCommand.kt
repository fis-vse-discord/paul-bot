package dev.vrba.paul.discord.modules.roles.commands

import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.commands.SlashCommand
import dev.vrba.paul.discord.commands.checks.RequireAdministratorPermissions
import dev.vrba.paul.discord.modules.roles.entities.RoleMenu
import dev.vrba.paul.discord.modules.roles.services.RoleMenusService
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.CommandData
import org.springframework.stereotype.Component
import java.time.Instant

@Component
@RequireAdministratorPermissions
class DeleteRoleMenuCommand(private val service: RoleMenusService) : SlashCommand {

    override val definition: CommandData = CommandData("delete-role-menu", "Delete role menu and all bound roles")
        .addOption(OptionType.STRING, "name", "Name of the role menu that should be deleted", true)

    override fun execute(event: SlashCommandEvent) {
        val name = event.getOption("name")?.asString ?: throw IllegalArgumentException("Missing the name parameter")

        val interaction = event.deferReply().complete()
        val menu = service.deleteRoleMenu(name)

        interaction.editOriginalEmbeds(roleMenuDeleted(menu)).queue()
    }

    private fun roleMenuDeleted(menu: RoleMenu): MessageEmbed = EmbedBuilder()
        .setTitle("Role menu `${menu.name}` deleted")
        .setColor(DiscordColors.green)
        .setTimestamp(Instant.now())
        .build()
}