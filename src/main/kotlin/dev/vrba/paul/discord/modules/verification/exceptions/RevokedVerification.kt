package dev.vrba.paul.discord.modules.verification.exceptions

class RevokedVerification : RuntimeException("Verification revoked by the server administrators.")