package dev.vrba.paul.discord.modules.roles.entities

import org.hibernate.Hibernate
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "roles", uniqueConstraints = [UniqueConstraint(columnNames = ["menu_id", "mapped_role_id"])])
data class Role(
    @Id
    @Column(name = "id")
    val id: UUID = UUID.randomUUID(),

    @Column(name = "mapped_role_id")
    val mappedRole: Long,

    @Column(name = "mapped_emoji")
    val mappedEmoji: String,

    @ManyToOne
    var menu: RoleMenu
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        if (other !is Role) return false

        return id == other.id
    }

    override fun hashCode(): Int = 0

    override fun toString(): String = this::class.simpleName + "(id = $id)"
}
