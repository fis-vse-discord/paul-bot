package dev.vrba.paul.discord.modules.roles.commands

import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.commands.SlashCommand
import dev.vrba.paul.discord.commands.checks.RequireAdministratorPermissions
import dev.vrba.paul.discord.modules.roles.entities.Role
import dev.vrba.paul.discord.modules.roles.services.RoleMenusService
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.CommandData
import org.springframework.stereotype.Component
import java.time.Instant

@Component
@RequireAdministratorPermissions
class BindRoleCommand(private val service: RoleMenusService) : SlashCommand {

    override val definition: CommandData = CommandData("bind-role", "Binds a role to the selected role menu")
        .addOption(OptionType.STRING, "name", "Name of the role menu, to which the role should be bound", true)
        .addOption(OptionType.ROLE, "role", "Role that should be bound to the role menu", true)

    override fun execute(event: SlashCommandEvent) {
        val name = event.getOption("name")?.asString ?: throw IllegalArgumentException("Missing the name parameter")
        val role = event.getOption("role")?.asRole ?: throw IllegalArgumentException("Missing the role parameter")

        val interaction = event.deferReply().complete()
        val bound = service.bindRoleToMenu(name, role)

        interaction.editOriginalEmbeds(roleBound(bound)).queue()
    }

    private fun roleBound(role: Role): MessageEmbed = EmbedBuilder()
        .setTitle("Role is now bound to the ${role.mappedEmoji} emoji")
        .setColor(DiscordColors.green)
        .setTimestamp(Instant.now())
        .build()

}