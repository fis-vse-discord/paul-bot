package dev.vrba.paul.discord.modules.strikes.repositories

import dev.vrba.paul.discord.modules.strikes.entities.Strike
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface StrikesRepository : CrudRepository<Strike, UUID> {

    fun countByMember(member: Long): Long

    fun findAllByMember(member: Long, pageable: Pageable): Page<Strike>
}