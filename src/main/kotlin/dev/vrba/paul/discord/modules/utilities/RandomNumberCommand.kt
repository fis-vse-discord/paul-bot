package dev.vrba.paul.discord.modules.utilities

import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.commands.SlashCommand
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.CommandData
import org.springframework.stereotype.Component
import java.time.Instant
import kotlin.random.Random
import kotlin.random.nextLong

@Component
class RandomNumberCommand : SlashCommand {

    override val definition: CommandData =
        CommandData("random-number", "Generates a random number in the specified boundaries.")
            .addOption(OptionType.INTEGER, "min", "Minimum (defaults to 0)", false)
            .addOption(OptionType.INTEGER, "max", "Maximum (defaults to 100)", false)
            .addOption(OptionType.BOOLEAN, "roll", "Rollin' dubs kek", false)

    override fun execute(event: SlashCommandEvent) {
        val min = event.getOption("min")?.asLong ?: 0L
        val max = event.getOption("max")?.asLong ?: 100L
        val roll = event.getOption("roll")?.asBoolean ?: false

        if (max < min) {
            return event.replyEmbeds(invalidNumberRange(min, max)).queue()
        }

        val embed = if (roll) rollingDubs(Random.nextLong(0L..99999999L))
                    else selectedNumber(Random.nextLong(min..max), min, max)

        event.replyEmbeds(embed).queue()
    }

    private fun invalidNumberRange(min: Long, max: Long): MessageEmbed = EmbedBuilder()
        .setTitle("Invalid number range `<$min - $max)`")
        .setTimestamp(Instant.now())
        .setColor(DiscordColors.red)
        .build()

    private fun selectedNumber(number: Long, min: Long, max: Long): MessageEmbed = EmbedBuilder()
        .setTitle("I've selected `$number`")
        .setTimestamp(Instant.now())
        .setFooter("Range: <$min - $max)")
        .setColor(DiscordColors.yellow)
        .build()

    private fun rollingDubs(number: Long): MessageEmbed = EmbedBuilder()
        .setTitle("\uD83C\uDF40 #`${String.format("%08d", number)}`")
        .setTimestamp(Instant.now())
        .setFooter("Rollin' dubs kek")
        .setColor(DiscordColors.internet)
        .build()
}