package dev.vrba.paul.discord.modules.roles.commands

import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.commands.SlashCommand
import dev.vrba.paul.discord.commands.checks.RequireAdministratorPermissions
import dev.vrba.paul.discord.modules.roles.entities.Role
import dev.vrba.paul.discord.modules.roles.services.RoleMenusService
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.CommandData
import org.springframework.stereotype.Component
import java.time.Instant

@Component
@RequireAdministratorPermissions
class UnbindRoleCommand(private val service: RoleMenusService) : SlashCommand {

    override val definition: CommandData = CommandData("unbind-role", "Removes a role binding from the specified role menu")
        .addOption(OptionType.STRING, "name", "Name of the role menu from which the role binding should be removed", true)
        .addOption(OptionType.ROLE, "role", "Role that should be removed from the role menu", true)


    override fun execute(event: SlashCommandEvent) {
        val name = event.getOption("name")?.asString ?: throw IllegalArgumentException("Missing the name parameter")
        val role = event.getOption("role")?.asRole ?: throw IllegalArgumentException("Missing the role parameter")

        val interaction = event.deferReply().complete()
        val bound = service.removeRoleBinding(name, role)

        interaction.editOriginalEmbeds(roleBindingRemoved(bound)).queue()
    }

    private fun roleBindingRemoved(role: Role): MessageEmbed = EmbedBuilder()
        .setTitle("Role binding removed")
        .setColor(DiscordColors.green)
        .setTimestamp(Instant.now())
        .build()
}