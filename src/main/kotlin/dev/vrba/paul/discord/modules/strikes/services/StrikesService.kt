package dev.vrba.paul.discord.modules.strikes.services

import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.modules.strikes.configuration.StrikesConfiguration
import dev.vrba.paul.discord.modules.strikes.entities.Strike
import dev.vrba.paul.discord.modules.strikes.repositories.StrikesRepository
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import java.time.Instant

@Service
class StrikesService(
    @Value("\${server.url}")
    private val url: String,
    private val repository: StrikesRepository,
    private val configuration: StrikesConfiguration,
    private val guild: Guild,
) {

    private val channel = guild.getTextChannelById(configuration.channel)
        ?: throw IllegalStateException("Cannot find the configured strikes channel (${configuration.channel})")

    fun createStrike(member: Member, admin: Member, reason: String): Strike {
        val count = repository.countByMember(member.idLong)
        val strike = Strike(member = member.idLong, admin = admin.idLong, index = count + 1, reason = reason)

        repository.save(strike)

        announceStrike(strike)

        if (strike.index == 3L) {
            guild.ban(member, 0, "Member received three strikes.").queue()
            announceBan(member)
        }

        return strike
    }

    fun removeStrike(member: Member, admin: Member): Strike {
        val page = PageRequest.of(0, 1, Sort.by(Sort.Direction.DESC, "index"))
        val strike = repository.findAllByMember(member.idLong, page).content.firstOrNull()
            ?: throw IllegalArgumentException("There are no strikes for the specified member (${member.id})")

        repository.delete(strike)

        announceStrikeRevoke(strike)

        if (strike.index == 3L) {
            guild.unban(member.id).queue()
            announceUnban(member)
        }

        return strike
    }

    private fun Long.toStrikeEmojis(): String =
        "\uD83D\uDFE5 ".repeat(this.toInt()) +
        "\u2B1B ".repeat(3 - this.toInt())

    private fun Long.toStrikeCount(): String = when (this) {
        1L -> "first"
        2L -> "second"
        else -> "last"
    }

    private fun announceStrike(strike: Strike) {
        val embed = EmbedBuilder()
            .setColor(DiscordColors.red)
            .setTitle("${strike.index.toStrikeEmojis()} Strike ${strike.index} / 3")
            .setImage("$url/assets/ban.png")
            .setDescription("<@${strike.member}> was given his ${strike.index.toStrikeCount()} strike!")
            .addField("Member", "<@${strike.member}>", false)
            .addField("Reason", strike.reason, false)
            .setTimestamp(Instant.now())
            .build()

        channel.sendMessageEmbeds(embed).queue()
    }

    private fun announceStrikeRevoke(strike: Strike) {
        val embed = EmbedBuilder()
            .setColor(DiscordColors.green)
            .setTitle("${(strike.index - 1).toStrikeEmojis()} Strike ${strike.index} / 3 revoked")
            .setDescription("The ${strike.index.toStrikeCount()} strike was revoked from <@${strike.member}>!")
            .addField("Member", "<@${strike.member}>", false)
            .setTimestamp(Instant.now())
            .build()

        channel.sendMessageEmbeds(embed).queue()
    }

    private fun announceBan(member: Member) {
        val embed = EmbedBuilder()
            .setColor(DiscordColors.red)
            .setTitle("Member was banned")
            .setDescription("This happens after a member receives 3 strikes.")
            .setImage("$url/assets/ban.png")
            .addField("Member banned", "<@${member.id}> (${member.effectiveName})", false)
            .setTimestamp(Instant.now())
            .build()

        channel.sendMessageEmbeds(embed).queue()
    }

    private fun announceUnban(member: Member) {
        val embed = EmbedBuilder()
            .setColor(DiscordColors.green)
            .setTitle("Member was unbanned")
            .setDescription("This makes Anthony happy")
            .addField("Member unbanned", "<@${member.id}> (${member.effectiveName})", false)
            .setTimestamp(Instant.now())
            .build()

        channel.sendMessageEmbeds(embed).queue()
    }
}