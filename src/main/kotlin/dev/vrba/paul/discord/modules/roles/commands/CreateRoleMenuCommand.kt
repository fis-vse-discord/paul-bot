package dev.vrba.paul.discord.modules.roles.commands

import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.commands.SlashCommand
import dev.vrba.paul.discord.commands.checks.RequireAdministratorPermissions
import dev.vrba.paul.discord.modules.roles.entities.RoleMenu
import dev.vrba.paul.discord.modules.roles.services.RoleMenusService
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.CommandData
import org.springframework.stereotype.Component
import java.time.Instant

@Component
@RequireAdministratorPermissions
class CreateRoleMenuCommand(private val service: RoleMenusService) : SlashCommand {

    override val definition: CommandData = CommandData("create-role-menu", "Creates a new role menu in the specified channel.")
        .addOption(OptionType.STRING, "name", "Name of the role menu (must be unique)", true)
        .addOption(OptionType.CHANNEL, "channel", "Channel, in which the role menu should be created", true)

    override fun execute(event: SlashCommandEvent) {
        val name = event.getOption("name")?.asString ?: throw IllegalArgumentException("Missing the name parameter")
        val channel = event.getOption("channel")?.asGuildChannel ?: throw IllegalArgumentException("Missing the channel parameter")

        if (channel !is TextChannel) {
            event.replyEmbeds(invalidChannelType()).queue()
            return
        }

        val interaction = event.deferReply().complete()
        val menu = service.createRoleMenu(name, channel)

        interaction.editOriginalEmbeds(roleMenuCreated(menu)).queue()
    }

    private fun invalidChannelType(): MessageEmbed = EmbedBuilder()
        .setTitle("Invalid channel type selected")
        .setColor(DiscordColors.red)
        .setDescription("The channel must be an instance of text channel")
        .setTimestamp(Instant.now())
        .build()

    private fun roleMenuCreated(menu: RoleMenu): MessageEmbed = EmbedBuilder()
        .setTitle("Role menu `${menu.name}` created")
        .setColor(DiscordColors.green)
        .setTimestamp(Instant.now())
        .build()
}