package dev.vrba.paul.discord.modules.subjects.commands

import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.commands.SlashCommand
import dev.vrba.paul.discord.commands.checks.RequireVerification
import dev.vrba.paul.discord.modules.subjects.repositories.SubjectsRepository
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.build.CommandData
import org.springframework.stereotype.Component
import java.time.Instant

@Component
@RequireVerification
class ListSubjectsCommand(private val repository: SubjectsRepository) : SlashCommand {

    override val definition: CommandData = CommandData("list-subjects", "Displays a list of all registered subjects")

    override fun execute(event: SlashCommandEvent) {
        val subjects = repository.findAll().joinToString(", ") { "`${it.code}`" }
        val embed = EmbedBuilder()
            .setTitle("Registered subjects")
            .setColor(DiscordColors.primary)
            .setDescription(subjects)
            .setTimestamp(Instant.now())
            .build()

        event.replyEmbeds(embed).queue()
    }
}