package dev.vrba.paul.discord.modules.socialcredit.handlers

import dev.vrba.paul.discord.DiscordLogger
import dev.vrba.paul.discord.modules.socialcredit.configuration.SocialCreditConfiguration
import dev.vrba.paul.discord.modules.socialcredit.entities.SocialCredit
import dev.vrba.paul.discord.modules.socialcredit.repositories.SocialCreditRepository
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.events.message.guild.react.GenericGuildMessageReactionEvent
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionRemoveEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import org.springframework.stereotype.Component

@Component
class SocialCreditReactionsHandler(
    private val configuration: SocialCreditConfiguration,
    private val repository: SocialCreditRepository,
    private val logger: DiscordLogger
) : ListenerAdapter() {

    // How much should the credit be affected by reaction
    private val score = mapOf(
        configuration.plusEmojiId to 1L,
        configuration.minusEmojiId to -1L
    )

    private val adminMultiplier = 5L

    override fun onGuildMessageReactionAdd(event: GuildMessageReactionAddEvent) {
        val member = event.retrieveMember().complete() ?: return
        val message = event.retrieveMessage().complete()

        if (shouldHandle(event, member, message)) {
            handle(event, member, message)
        }
    }

    override fun onGuildMessageReactionRemove(event: GuildMessageReactionRemoveEvent) {
        val member = event.retrieveMember().complete() ?: return
        val message = event.retrieveMessage().complete()

        if (shouldHandle(event, member, message)) {
            handle(event, member, message)
        }
    }

    private fun handle(event: GenericGuildMessageReactionEvent, member: Member, message: Message) {
        val author = message.author.idLong
        val emote = event.reactionEmote.emote.idLong

        val diff = score[emote] ?: throw IllegalStateException("No rank set for given reaction emoji ($emote)")
        val multiplier = if (member.hasPermission(Permission.MANAGE_SERVER)) adminMultiplier else 1
        val direction = if (event is GuildMessageReactionAddEvent) 1 else -1

        val entity = repository.findByMember(author) ?: SocialCredit(member = author, credit = configuration.defaultCredit)

        val original = entity.credit
        val updated = entity.credit + (diff * multiplier * direction)

        repository.save(entity.apply { credit = updated })

        // The credit is applied already here
        val title = "Social credit of **${message.author.name.replace("@", "")}** changed from `$original` to `$updated`"
        val description = "Reaction author = **${member.effectiveName.replace("@", "")}**, Message link = ${message.jumpUrl}"

        if (original < updated) logger.successBasic(title, description)
        else logger.errorBasic(title, description)
    }

    private fun shouldHandle(event: GenericGuildMessageReactionEvent, member: Member, message: Message): Boolean =
        event.reactionEmote.isEmote &&
        event.reactionEmote.emote.idLong in score.keys &&
        // The user cannot give / remove social credits from himself
        message.author.id != member.id
}