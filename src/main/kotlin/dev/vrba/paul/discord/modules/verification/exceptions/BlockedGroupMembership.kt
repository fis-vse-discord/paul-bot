package dev.vrba.paul.discord.modules.verification.exceptions

class BlockedGroupMembership : RuntimeException("User is member of one or more blocked Azure AD groups.")