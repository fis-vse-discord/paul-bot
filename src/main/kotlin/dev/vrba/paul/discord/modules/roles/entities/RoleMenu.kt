package dev.vrba.paul.discord.modules.roles.entities

import org.hibernate.Hibernate
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "role_menu")
data class RoleMenu(
    @Id
    @Column(name = "id")
    val id: UUID = UUID.randomUUID(),

    @Column(name = "name", unique = true)
    val name: String,

    @Column(name = "channel_id")
    val channelId: Long,

    @Column(name = "message_id")
    val messageId: Long,

    @JoinColumn(name = "menu_id")
    @OneToMany(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    var mappedRoles: List<Role> = listOf()
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        if (other !is RoleMenu) return false

        return id == other.id
    }

    override fun hashCode(): Int = 0

    override fun toString(): String = this::class.simpleName + "(id = $id)"
}