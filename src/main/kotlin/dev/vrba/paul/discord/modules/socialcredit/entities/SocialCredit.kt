package dev.vrba.paul.discord.modules.socialcredit.entities

import org.hibernate.Hibernate
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "social_credit")
data class SocialCredit(
    @Id
    val id: UUID = UUID.randomUUID(),

    @Column(unique = true)
    val member: Long,

    var credit: Long
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        if (other !is SocialCredit) return false

        return id == other.id
    }

    override fun hashCode(): Int = 0

    override fun toString(): String = this::class.simpleName + "(id = $id)"
}