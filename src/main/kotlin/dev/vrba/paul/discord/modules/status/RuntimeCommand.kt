package dev.vrba.paul.discord.modules.status

import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.commands.SlashCommand
import dev.vrba.paul.discord.commands.checks.RequireAdministratorPermissions
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.build.CommandData
import org.springframework.stereotype.Component
import java.time.Instant
import kotlin.math.round

@Component
@RequireAdministratorPermissions
class RuntimeCommand : SlashCommand {

    override val definition: CommandData =
        CommandData("runtime", "Provides runtime information about the currently deployed instance")

    override fun execute(event: SlashCommandEvent) {
        val interaction = event.deferReply().complete()
        val runtime = Runtime.getRuntime() ?: throw IllegalStateException("Cannot get the Runtime.getRuntime() instance")

        interaction.editOriginalEmbeds(runtimeEmbed(runtime)).queue()
    }

    private fun runtimeEmbed(runtime: Runtime): MessageEmbed {
        // Fixes a value at the invocation time
        val totalMemory = runtime.totalMemory()
        val usedMemory = totalMemory - runtime.freeMemory()
        val percentage = usedMemory / totalMemory.toDouble()

        val color = when (percentage) {
            in 0.0..0.5 -> DiscordColors.green
            in 0.5..0.75 -> DiscordColors.yellow
            else -> DiscordColors.red
        }

        return EmbedBuilder()
            .setColor(color)
            .setTitle("Runtime information")
            .addField("Available CPUs", "`${runtime.availableProcessors()}`", false)
            .addField("Current JVM memory usage", "`$usedMemory` / `$totalMemory` **(${round(percentage * 100)}%)**", false)
            .addField("Max JVM memory", "`${runtime.maxMemory()}`", false)
            .setTimestamp(Instant.now())
            .build()
    }
}