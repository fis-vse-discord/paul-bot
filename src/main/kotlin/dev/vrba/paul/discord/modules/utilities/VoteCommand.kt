package dev.vrba.paul.discord.modules.utilities

import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.commands.SlashCommand
import dev.vrba.paul.discord.commands.checks.RequireVerification
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.CommandData
import org.springframework.stereotype.Component
import java.time.Instant

@Component
@RequireVerification
class VoteCommand : SlashCommand {
    override val definition: CommandData = CommandData("vote", "Creates a yes / no voting poll")
        .addOption(OptionType.STRING, "question", "Question displayed in the poll title", true)

    override fun execute(event: SlashCommandEvent) {
        val question = event.getOption("question")?.asString ?: throw IllegalArgumentException("Missing the question parameter")
        val embed = EmbedBuilder()
            .setAuthor(event.user.name, null, event.user.effectiveAvatarUrl)
            .setTitle(question)
            .setColor(DiscordColors.yellow)
            .setTimestamp(Instant.now())
            .build()

        val interaction = event.replyEmbeds(embed).complete()
        val message = interaction.retrieveOriginal().complete()

        message.addReaction("\uD83D\uDC4D").queue()
        message.addReaction("\uD83D\uDC4E").queue()
    }
}