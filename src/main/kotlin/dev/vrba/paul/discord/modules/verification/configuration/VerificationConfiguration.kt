package dev.vrba.paul.discord.modules.verification.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "verification")
data class VerificationConfiguration(
    val verifiedRole: Long,
    val verificationChannel: Long,
    val verificationMessage: Long,
    val verificationEmoji: Long,
    val blockedRoles: List<String>
)
