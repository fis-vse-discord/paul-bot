package dev.vrba.paul.discord.modules.subjects.commands

import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.commands.SlashCommand
import dev.vrba.paul.discord.commands.checks.RequireAdministratorPermissions
import dev.vrba.paul.discord.modules.subjects.entities.Subject
import dev.vrba.paul.discord.modules.subjects.services.SubjectsService
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.CommandData
import org.springframework.stereotype.Component
import java.time.Instant

@Component
@RequireAdministratorPermissions
class DeleteSubjectCommand(private val service: SubjectsService) : SlashCommand {

    override val definition: CommandData = CommandData("delete-subject", "Delete a subject + associated role and channels")
        .addOption(OptionType.STRING, "code", "Code of the subject that should be deleted", true)

    override fun execute(event: SlashCommandEvent) {
        val code = event.getOption("code")?.asString ?: throw IllegalArgumentException("Missing the code parameter")

        val interaction = event.deferReply().complete()
        val subject = service.deleteSubject(code)

        interaction.editOriginalEmbeds(subjectDeleted(subject)).queue()
    }

    private fun subjectDeleted(subject: Subject): MessageEmbed = EmbedBuilder()
        .setTitle("Subject `${subject.code}` deleted.")
        .setColor(DiscordColors.green)
        .setTimestamp(Instant.now())
        .build()
}