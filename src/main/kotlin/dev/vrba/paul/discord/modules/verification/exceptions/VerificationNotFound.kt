package dev.vrba.paul.discord.modules.verification.exceptions

class VerificationNotFound : RuntimeException("Verification not found.")