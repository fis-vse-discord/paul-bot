package dev.vrba.paul.discord.modules.verification.exceptions

class AzureIdMismatch : RuntimeException("Azure ID mismatch. This verification has been used by another user already.")