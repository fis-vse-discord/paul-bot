package dev.vrba.paul.discord.modules.roles.repositories

import dev.vrba.paul.discord.modules.roles.entities.Role
import dev.vrba.paul.discord.modules.roles.entities.RoleMenu
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface RolesRepository : CrudRepository<Role, UUID> {

    fun countByMenu(menu: RoleMenu): Int

    fun findAllByMenu(menu: RoleMenu): List<Role>

    fun findByMenuAndMappedRole(menu: RoleMenu, mappedRole: Long): Role?

    fun findByMenuAndMappedEmoji(menu: RoleMenu, mappedEmoji: String): Role?
}