package dev.vrba.paul.discord.modules.verification.handlers

import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.modules.verification.configuration.VerificationConfiguration
import dev.vrba.paul.discord.modules.verification.services.VerificationsService
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import net.dv8tion.jda.api.interactions.components.Button
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class VerificationReactionAddedHandler(
    @Value("\${server.url}")
    private val baseUrl: String,
    private val service: VerificationsService,
    private val configuration: VerificationConfiguration,
) : ListenerAdapter() {

    override fun onGuildMessageReactionAdd(event: GuildMessageReactionAddEvent) {
        if (shouldHandle(event)) {
            val verification = service.getMemberVerification(event.userIdLong)
            val link = "$baseUrl/verification/${verification.id}"
            val embed = createVerificationEmbed(link)

            event.retrieveUser().complete()
                .openPrivateChannel().complete()
                .sendMessageEmbeds(embed)
                .setActionRow(Button.link(link, "\uD83D\uDC49 Tvůj unikátní odkaz"))
                .queue()
        }
    }

    private fun shouldHandle(event: GuildMessageReactionAddEvent): Boolean =
        event.channel.idLong == configuration.verificationChannel &&
        event.messageIdLong == configuration.verificationMessage &&
        event.reactionEmote.idLong == configuration.verificationEmoji

    private fun createVerificationEmbed(link: String): MessageEmbed = EmbedBuilder()
        .setTitle("Verifikace VŠE Účtu", link)
        .setDescription(
            """
            |Pro verifikaci klikni na tlačítko pod zprávou a budeš vyzván k přihlášení pomocí školního Microsoft účtu.
            |
            |Pro přihlášení použij školní adresu ve tvaru `xname@vse.cz`
            |
            |V případě jakýchkoliv problémů s ověřením prosím kontaktuj adminy serveru.
           """.trimMargin()
        )
        .setColor(DiscordColors.vse)
        .setTimestamp(Instant.now())
        .build()
}