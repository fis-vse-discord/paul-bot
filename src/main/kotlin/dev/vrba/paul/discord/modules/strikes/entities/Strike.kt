package dev.vrba.paul.discord.modules.strikes.entities

import org.hibernate.Hibernate
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "strikes")
data class Strike(
    @Id
    val id: UUID = UUID.randomUUID(),

    val member: Long,

    val admin: Long,

    val index: Long,

    val reason: String,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        if (other !is Strike) return false

        return id == other.id
    }

    override fun hashCode(): Int = 0

    override fun toString(): String = this::class.simpleName + "(id = $id)"
}