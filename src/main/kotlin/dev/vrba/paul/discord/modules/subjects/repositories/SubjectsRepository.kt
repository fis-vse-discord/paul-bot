package dev.vrba.paul.discord.modules.subjects.repositories

import dev.vrba.paul.discord.modules.subjects.entities.Subject
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface SubjectsRepository : CrudRepository<Subject, UUID> {

    fun existsByCode(code: String): Boolean

    fun findByCode(code: String): Subject?
}