package dev.vrba.paul.discord.modules.subjects.services

import dev.vrba.paul.discord.DiscordLogger
import dev.vrba.paul.extensions.everyone
import dev.vrba.paul.discord.modules.subjects.configuration.SubjectsConfiguration
import dev.vrba.paul.discord.modules.subjects.entities.Subject
import dev.vrba.paul.discord.modules.subjects.repositories.SubjectsRepository
import net.dv8tion.jda.api.Permission.*
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Role
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.entities.VoiceChannel
import org.springframework.stereotype.Service

@Service
class SubjectsService(
    configuration: SubjectsConfiguration,
    private val guild: Guild,
    private val logger: DiscordLogger,
    private val repository: SubjectsRepository,
) {

    private val textChannelsCategory = guild.getCategoryById(configuration.textChannelsCategory)
        ?: throw IllegalStateException("Cannot find the configured text channels category (${configuration.textChannelsCategory})")

    private val voiceChannelsCategory = guild.getCategoryById(configuration.voiceChannelsCategory)
        ?: throw IllegalStateException("Cannot find the configured voice channels category (${configuration.voiceChannelsCategory})")

    fun createSubject(code: String, name: String): Subject {
        val role = createRole(code, name)
        val textChannel = createTextChannel(code, name, role)
        val voiceChannel = createVoiceChannel(code, name, role)

        val subject = Subject(
            code = code,
            name = name,
            roleId = role.idLong,
            textChannelId = textChannel.idLong,
            voiceChannelId = voiceChannel.idLong
        )

        repository.save(subject)

        logger.success("Subject `$code` created", "**$name**") {
            it.addField("Text channel", "<#${subject.textChannelId}>", false)
                .addField("Voice channel", "<#${subject.voiceChannelId}>", false)
                .addField("Role", "<@&${subject.roleId}>", false)
        }

        return subject
    }

    fun deleteSubject(code: String): Subject {
        val subject = repository.findByCode(code) ?: throw IllegalArgumentException("Subject with code $code not found.")

        guild.getTextChannelById(subject.textChannelId)?.delete()?.queue()
        guild.getVoiceChannelById(subject.textChannelId)?.delete()?.queue()
        guild.getRoleById(subject.roleId)?.delete()?.queue()

        repository.delete(subject)

        logger.success("Subject `$code` deleted", "**${subject.name}**") {
            it.addField("Text channel", "<#${subject.textChannelId}>", false)
                .addField("Voice channel", "<#${subject.voiceChannelId}>", false)
                .addField("Role", "<@&${subject.roleId}>", false)
        }

        return subject
    }

    private fun createRole(code: String, name: String): Role =
        guild.createRole()
            .setName("$code - $name")
            .setMentionable(false)
            .setHoisted(false)
            .complete()

    private fun createTextChannel(code: String, name: String, role: Role): TextChannel =
        guild.createTextChannel("$code - $name", textChannelsCategory)
            .addPermissionOverride(role.guild.everyone, emptyList(), listOf(VIEW_CHANNEL))
            .addPermissionOverride(role, listOf(VIEW_CHANNEL, MESSAGE_READ, MESSAGE_WRITE, MESSAGE_HISTORY), emptyList())
            .setTopic(name)
            .complete()

    private fun createVoiceChannel(code: String, name: String, role: Role): VoiceChannel =
        guild.createVoiceChannel("$code - $name", voiceChannelsCategory)
            .addPermissionOverride(role.guild.everyone, emptyList(), listOf(VIEW_CHANNEL, VOICE_CONNECT))
            .addPermissionOverride(role, listOf(VIEW_CHANNEL, VOICE_CONNECT, VOICE_SPEAK), emptyList())
            .complete()
}