package dev.vrba.paul.discord.modules.socialcredit.repositories

import dev.vrba.paul.discord.modules.socialcredit.entities.SocialCredit
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface SocialCreditRepository : CrudRepository<SocialCredit, UUID> {

    fun findByMember(member: Long): SocialCredit?

    fun findAll(pageable: Pageable): Page<SocialCredit>
}