package dev.vrba.paul.discord.modules.roles.services

import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.DiscordLogger
import dev.vrba.paul.discord.modules.roles.entities.Role as MappedRole
import dev.vrba.paul.discord.modules.roles.entities.RoleMenu
import dev.vrba.paul.discord.modules.roles.repositories.RoleMenusRepository
import dev.vrba.paul.discord.modules.roles.repositories.RolesRepository
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.*
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import org.springframework.stereotype.Service
import java.time.Instant

@Service
class RoleMenusService(
    private val guild: Guild,
    private val logger: DiscordLogger,
    private val menusRepository: RoleMenusRepository,
    private val rolesRepository: RolesRepository
) : ListenerAdapter() {

    // There is a limit of 20 reactions / message
    // TODO: Maybe add different emoji that are a little bit more distinguishable
    private val reactionEmoji = listOf(
        "🇦", "🇧", "🇨", "🇩",
        "🇪", "🇫", "🇬", "🇭",
        "🇮", "🇯", "🇰", "🇱",
        "🇲", "🇳", "🇴", "🇵",
        "🇶", "🇷", "🇸", "🇹"
    )

    fun createRoleMenu(name: String, channel: TextChannel): RoleMenu {
        val message = channel.sendMessage("_Role menu placeholder_").complete()
        val menu = RoleMenu(name = name, channelId = channel.idLong, messageId = message.idLong)

        menusRepository.save(menu)
        logger.success("Role menu `${menu.name} created`", null) {
            it.addField("Channel", "<#${menu.channelId}>", false)
                .addField("Message", message.jumpUrl, false)
        }

        updateRoleMenuEmbed(menu, message)

        return menu
    }

    fun deleteRoleMenu(name: String): RoleMenu {
        val menu =
            menusRepository.findByName(name) ?: throw IllegalArgumentException("Role menu with name `$name` not found")
        val message = guild.getTextChannelById(menu.channelId)?.retrieveMessageById(menu.messageId)?.complete()
            ?: throw IllegalArgumentException("Message not found for the role menu `$name`")

        menusRepository.delete(menu)
        message.delete().queue()

        logger.success("Role menu `$name` deleted")

        return menu
    }

    fun bindRoleToMenu(name: String, role: Role): MappedRole {
        val menu =
            menusRepository.findByName(name) ?: throw IllegalArgumentException("Role menu with name `$name` not found.")
        val usedEmojis = rolesRepository.findAllByMenu(menu).map { it.mappedEmoji }

        // Find first unused emoji
        val emoji = reactionEmoji.firstOrNull { it !in usedEmojis }
            ?: throw IllegalStateException("There are already ${reactionEmoji.size} roles bound to this menu.")
        val bound = MappedRole(mappedRole = role.idLong, mappedEmoji = emoji, menu = menu)

        rolesRepository.save(bound)
        menusRepository.save(menu)

        logger.info("Created a role binding in role menu", null) {
            it.addField("Role menu", "**${menu.name}** (`${menu.id}`)", false)
                .addField("Role added", "<@&${bound.mappedRole}> (${bound.mappedEmoji})", false)
        }

        updateRoleMenuEmbed(menu)

        return bound
    }

    fun removeRoleBinding(name: String, role: Role): MappedRole {
        val menu =
            menusRepository.findByName(name) ?: throw IllegalArgumentException("Role menu with name `$name` not found.")
        val bound = rolesRepository.findByMenuAndMappedRole(menu, role.idLong)
            ?: throw IllegalArgumentException("Role binding for `$role` not found.")

        rolesRepository.delete(bound)
        menusRepository.save(menu)

        logger.info("Removed a role binding from role menu", null) {
            it.addField("Role menu", "**${menu.name}** (`${menu.id}`)", false)
                .addField("Role removed", "<@&${bound.mappedRole}> (${bound.mappedEmoji})", false)
        }

        updateRoleMenuEmbed(menu)

        return bound
    }

    private fun updateRoleMenuEmbed(menu: RoleMenu, message: Message? = null) {
        @Suppress("NAME_SHADOWING")
        val message =
            message ?: guild.getTextChannelById(menu.channelId)?.retrieveMessageById(menu.messageId)?.complete()
            ?: throw IllegalStateException("Cannot find the role menu message")

        val roles = rolesRepository.findAllByMenu(menu).sortedBy { it.mappedEmoji }

        val embed = EmbedBuilder()
            .setTitle(menu.name)
            .setColor(DiscordColors.fuchsia)
            .setDescription(
                "Po kliknutí na reakci pod zprávou ti bot přidělí / odebere příslušnou roli\n\n" +
                        roles.joinToString("\n\n") { "${it.mappedEmoji}: <@&${it.mappedRole}>" }
            )
            .setTimestamp(Instant.now())
            .build()

        message.editMessage(" ").setEmbeds(embed).queue()
        message.clearReactions().complete()

        roles.forEach { message.addReaction(it.mappedEmoji).queue() }
    }

    override fun onGuildMessageReactionAdd(event: GuildMessageReactionAddEvent) {
        val user = event.retrieveUser().complete() ?: throw IllegalStateException("Cannot retrieve the reaction user")

        // Some optimizations to save a database call
        if (!event.reactionEmote.isEmoji || event.reactionEmote.emoji !in reactionEmoji || user.isBot) {
            return;
        }

        val emoji = event.reactionEmote.emoji
        val menu = menusRepository.findByChannelIdAndMessageId(event.channel.idLong, event.messageIdLong) ?: return
        val role = rolesRepository.findByMenuAndMappedEmoji(menu, emoji)
            ?: throw IllegalStateException("Reaction added to a valid role menu, but a role mapped to $emoji not found ")

        event.reaction.removeReaction(user).queue()

        toggleUserRole(user, role)
    }

    private fun toggleUserRole(user: User, entity: MappedRole) {
        val member = guild.retrieveMember(user).complete() ?: throw IllegalStateException("Cannot find the member")
        val role = guild.getRoleById(entity.mappedRole) ?: throw IllegalStateException("Cannot find the role")

        if (member.roles.contains(role)) {
            guild.removeRoleFromMember(member, role).queue()
            logger.errorBasic("Removed self-assignable role **${role.name}** from **${member.effectiveName.replace("@", "")}**")

            return
        }

        guild.addRoleToMember(member, role).queue()
        logger.successBasic("Added self-assignable role **${role.name}** from **${member.effectiveName.replace("@", "")}**")
    }
}