package dev.vrba.paul.discord.modules.socialcredit.commands

import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.commands.SlashCommand
import dev.vrba.paul.discord.commands.checks.RequireVerification
import dev.vrba.paul.discord.modules.socialcredit.entities.SocialCredit
import dev.vrba.paul.discord.modules.socialcredit.repositories.SocialCreditRepository
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.CommandData
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Component
import java.time.Instant

@Component
@RequireVerification
class SocialCreditLeaderboardCommand(private val repository: SocialCreditRepository) : SlashCommand {

    override val definition: CommandData = CommandData("social-credit-leaderboard", "Displays a leaderboard based on members social credit")
        .addOption(OptionType.INTEGER, "count", "Number of entries displayed for each group, must be between 1 and 20, defaults to 5", false)

    override fun execute(event: SlashCommandEvent) {
        val count = event.getOption("count")?.asLong?.toInt() ?: 5

        if (count !in 1..20) {
            throw IndexOutOfBoundsException("Number of entries must be between 1 and 20")
        }

        val best = repository.findAll(PageRequest.of(0, count, Sort.by(Sort.Order.desc("credit"))))
        val worst = repository.findAll(PageRequest.of(0, count, Sort.by(Sort.Order.asc("credit"))))

        val embed = createLeaderboardEmbed(best, worst, count)

        event.replyEmbeds(embed).queue()
    }

    @Suppress("NAME_SHADOWING")
    private fun createLeaderboardEmbed(best: Page<SocialCredit>, worst: Page<SocialCredit>, count: Int): MessageEmbed {
        val best = best.content.joinToString("\n") { "`${it.credit.toString().padStart(4, ' ')}` <@${it.member}>" }
        val worst = worst.content.joinToString("\n") { "`${it.credit.toString().padStart(4, ' ')}` <@${it.member}>" }

        return EmbedBuilder()
            .setTitle("Social credit leaderboard")
            .setColor(DiscordColors.primary)
            .setTimestamp(Instant.now())
            .addField("\uD83D\uDC4D Top $count good citizens", best, true)
            .addField("\uD83D\uDC4E Top $count bad citizens", worst, true)
            .build()
    }
}