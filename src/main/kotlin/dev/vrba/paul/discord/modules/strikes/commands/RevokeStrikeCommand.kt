package dev.vrba.paul.discord.modules.strikes.commands

import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.commands.SlashCommand
import dev.vrba.paul.discord.commands.checks.RequireAdministratorPermissions
import dev.vrba.paul.discord.modules.strikes.entities.Strike
import dev.vrba.paul.discord.modules.strikes.services.StrikesService
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.CommandData
import org.springframework.stereotype.Component
import java.time.Instant

@Component
@RequireAdministratorPermissions
class RevokeStrikeCommand(private val service: StrikesService) : SlashCommand {

    override val definition: CommandData = CommandData("revoke-strike", "Removes the last strike from the specified member")
        .addOption(OptionType.USER, "member", "Member from which the strike should be removed", true)

    override fun execute(event: SlashCommandEvent) {
        val admin = event.member ?: throw IllegalStateException("Missing the member property on the SlashCommandEvent instance")
        val member = event.getOption("member")?.asMember ?: throw IllegalArgumentException("Missing the member parameter")

        val interaction = event.deferReply().complete()
        val strike = service.removeStrike(member, admin)

        interaction.editOriginalEmbeds(strikeRevokedEmbed(strike)).queue()
    }

    fun strikeRevokedEmbed(strike: Strike): MessageEmbed = EmbedBuilder()
        .setTitle("Strike ${strike.index} / 3 revoked")
        .setColor(DiscordColors.green)
        .addField("Member", "<@${strike.member}>", true)
        .addField("Admin", "<@${strike.admin}>", true)
        .setTimestamp(Instant.now())
        .build()
}