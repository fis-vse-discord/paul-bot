package dev.vrba.paul.discord.modules.verification.controllers

import dev.vrba.paul.discord.modules.verification.services.VerificationsService
import org.springframework.security.access.annotation.Secured
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.security.oauth2.core.user.OAuth2User
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import java.util.*

@Controller
@RequestMapping("/verification")
class VerificationsController(private val service: VerificationsService) {

    @Secured("ROLE_USER")
    @GetMapping("/{id}")
    fun process(@PathVariable id: UUID, @AuthenticationPrincipal user: OAuth2User): String {
        if (service.processMemberVerification(id, user)) {
            return "success"
        }

        return "failure"
    }
}