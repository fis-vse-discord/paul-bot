package dev.vrba.paul.discord.modules.verification.repositories

import dev.vrba.paul.discord.modules.verification.entities.Verification
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface VerificationsRepository : CrudRepository<Verification, UUID> {
    fun findByMemberId(memberId: Long): Verification?
}