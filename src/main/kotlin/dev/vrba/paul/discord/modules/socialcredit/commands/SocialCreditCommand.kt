package dev.vrba.paul.discord.modules.socialcredit.commands

import dev.vrba.paul.discord.DiscordColors
import dev.vrba.paul.discord.commands.SlashCommand
import dev.vrba.paul.discord.commands.checks.RequireVerification
import dev.vrba.paul.discord.modules.socialcredit.configuration.SocialCreditConfiguration
import dev.vrba.paul.discord.modules.socialcredit.repositories.SocialCreditRepository
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.CommandData
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.time.Instant

@Component
@RequireVerification
class SocialCreditCommand(
    @Value("\${server.url}")
    private val url: String,
    private val configuration: SocialCreditConfiguration,
    private val repository: SocialCreditRepository
) : SlashCommand {

    override val definition: CommandData =
        CommandData("social-credit", "Displays the social credit for the selected member")
            .addOption(OptionType.USER, "member", "Member which credit should be displayed", true)

    override fun execute(event: SlashCommandEvent) {
        val member = event.getOption("member")?.asMember ?: throw IllegalArgumentException("Missing the member parameter")
        val credit = repository.findByMember(member.idLong)?.credit ?: configuration.defaultCredit

        val embed = socialCreditEmbed(member, credit)

        event.replyEmbeds(embed).queue()
    }

    private fun socialCreditEmbed(member: Member, credit: Long): MessageEmbed {
        val (color, thumbnail, description) =
            if (credit >= 0) Triple(DiscordColors.green, "$url/assets/anthony.png", "Anthony is proud of you")
            else Triple(DiscordColors.red, "$url/assets/radvakova.png","Date of execution sent to Radváková")

        return EmbedBuilder()
            .setColor(color)
            .setAuthor(member.effectiveName, null, member.user.effectiveAvatarUrl)
            .setTitle("Social credit = `$credit`")
            .setThumbnail(thumbnail)
            .setDescription(description)
            .setTimestamp(Instant.now())
            .build()
    }
}