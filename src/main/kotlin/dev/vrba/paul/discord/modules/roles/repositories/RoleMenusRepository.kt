package dev.vrba.paul.discord.modules.roles.repositories

import dev.vrba.paul.discord.modules.roles.entities.RoleMenu
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface RoleMenusRepository : CrudRepository<RoleMenu, UUID> {

    fun existsByName(name: String): Boolean

    fun findByName(name: String): RoleMenu?

    fun findByChannelIdAndMessageId(channelId: Long, messageId: Long): RoleMenu?
}