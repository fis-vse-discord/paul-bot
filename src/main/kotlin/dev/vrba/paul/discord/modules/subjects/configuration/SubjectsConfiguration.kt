package dev.vrba.paul.discord.modules.subjects.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "subjects")
data class SubjectsConfiguration(
    val textChannelsCategory: Long,
    val voiceChannelsCategory: Long,
)
