package dev.vrba.paul.discord.modules.subjects.entities

import org.hibernate.Hibernate
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "subject")
data class Subject(
    @Id
    @Column(name = "id")
    val id: UUID = UUID.randomUUID(),

    @Column(name = "code", unique = true)
    val code: String,

    @Column(name = "name")
    val name: String,

    @Column(name = "role_id")
    val roleId: Long,

    @Column(name = "text_channel_id")
    val textChannelId: Long,

    @Column(name = "voice_channel_id")
    val voiceChannelId: Long
) {
    // Methods optimized for Hibernate performance

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        if (other !is Subject) return false

        return id == other.id
    }

    override fun hashCode(): Int = 0

    override fun toString(): String = this::class.simpleName + "(id = $id)"
}