package dev.vrba.paul.discord

// https://discord.com/branding
object DiscordColors {
    const val primary = 0x5865f2
    const val green = 0x57f287
    const val yellow = 0xfee75c
    const val fuchsia = 0xeb459e
    const val red = 0xed4245
    const val vse = 0x009ee0
    const val internet = 0x66cc33
    const val git = 0xf54d27
}