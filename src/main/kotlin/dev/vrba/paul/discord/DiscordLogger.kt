package dev.vrba.paul.discord

import dev.vrba.paul.discord.configuration.DiscordConfiguration
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.MessageEmbed
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.time.Instant

typealias EmbedConfigurer = (EmbedBuilder) -> EmbedBuilder

class DiscordLogger(configuration: DiscordConfiguration, guild: Guild) {

    private val channel = guild.getTextChannelById(configuration.logChannel)
        ?: throw IllegalStateException("Cannot find the configured log channel (${configuration.logChannel}).")

    fun info(title: String, description: String? = null, config: EmbedConfigurer? = null) =
        log(DiscordColors.primary, title, description, config)

    fun warning(title: String, description: String? = null, config: EmbedConfigurer? = null) =
        log(DiscordColors.yellow, title, description, config)

    fun error(title: String, description: String? = null, config: EmbedConfigurer? = null) =
        log(DiscordColors.red, title, description, config)

    fun success(title: String, description: String? = null, config: EmbedConfigurer? = null) =
        log(DiscordColors.green, title, description, config)

    fun infoBasic(title: String, description: String? = null) = logBasic("\uD83D\uDFE6", title, description)

    fun warningBasic(title: String, description: String? = null) = logBasic("\uD83D\uDFE8", title, description)

    fun errorBasic(title: String, description: String? = null) = logBasic("\uD83D\uDFE5", title, description)

    fun successBasic(title: String, description: String? = null) = logBasic("\uD83D\uDFE9", title, description)

    fun exception(exception: Throwable) {
        val message = channel.sendMessageEmbeds(
            buildMessageEmbed(
                DiscordColors.red,
                "`${exception::class.qualifiedName}`",
                "```${exception.message}```"
            )
        )

        try {
            val file = File.createTempFile("stacktrace", ".txt")
            val writer = BufferedWriter(FileWriter(file))
            val stacktrace = exception.stackTraceToString()

            writer.use {
                it.write(stacktrace)
                it.close()
            }

            message.addFile(file)
        }
        catch (exception: Exception) {
            exception.printStackTrace()
        }

        message.queue()
    }

    private fun log(color: Int, title: String, description: String? = null, config: EmbedConfigurer? = null) {
        channel.sendMessageEmbeds(buildMessageEmbed(color, title, description, config)).queue()
    }

    private fun logBasic(prefix: String, title: String, description: String? = null) {
        channel.sendMessage("$prefix $title ${description?.let{ "\n>$it" } ?: ""}").queue()


    }

    private fun buildMessageEmbed(color: Int, title: String, description: String? = null, config: EmbedConfigurer? = null): MessageEmbed {
        val builder = EmbedBuilder()
            .setColor(color)
            .setTitle(title)
            .setDescription(description ?: "")
            .setFooter(this::class.qualifiedName)
            .setTimestamp(Instant.now())

        return if (config == null) builder.build()
               else config(builder).build()
    }
}