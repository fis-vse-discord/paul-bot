package dev.vrba.paul

import dev.vrba.paul.discord.DiscordService
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan(basePackages = ["dev.vrba.paul"])
class PaulApplication(private val service: DiscordService) : CommandLineRunner {

    override fun run(vararg args: String) {
        service.start()
    }

}

fun main(args: Array<String>) {
    runApplication<PaulApplication>(*args)
}
